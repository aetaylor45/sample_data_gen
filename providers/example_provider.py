from faker.providers import BaseProvider
from random import randint


class ExampleProvider(BaseProvider):
    def random_percent(self):
        return randint(1, 100)
