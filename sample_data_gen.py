import configparser
import logging
import sys

import yaml
import json
import pandas as pd
import re
import inspect

from importlib import import_module
from os import walk
from os.path import isfile, exists, splitext
from faker import Faker
from faker.providers import BaseProvider

_DEFAULT_CONFIG = "conf.ini"
_DEFAULT_GENERATOR = "base_generator"
_DEFAULT_COUNT = 10


def collect_modules(package_path):
    """
    Collects all non-init .py files in a given directory.

    :param package_path: the directory to search for modules in
    :return:package: a list of potentially suitable .py files
    """
    package = []
    for (root, dirs, files) in walk(package_path):
        package.extend(files)
        break

    for file in package:
        filepath = splitext(file)
        if not filepath[-1] == ".py" or file.startswith("__"):
            del file

    return package


def get_package_from_path(package_path):
    """
    Converts a file path into a valid python import statement.

    :param package_path: the directory string to modify
    :return: a valid import statement
    """
    return re.sub(r"^\W+", "", package_path).replace("/", ".")


class SampleDataGenerator:
    """
    A small data generator that uses Faker and a set of template files to create data samples.
    """
    def __init__(self, ini_file=None):
        """
        Initializes all required file pathways, providers, and generators.

        :param ini_file: the location of the current ini.file
        """
        self.config = configparser.ConfigParser()
        self.config_path = ini_file if ini_file else _DEFAULT_CONFIG

        # Cannot run the generator if conf.ini is invalid
        if not isfile(self.config_path) or not exists(self.config_path):
            logging.warning("Unable to file .ini file {}".format(self.config_path))
            sys.exit()
        self.config.read(self.config_path)

        self.output_path = self.config['DEFAULT']['output_dir']
        self.template_path = self.config['DEFAULT']['templates_dir']
        self.providers_path = self.config['DEFAULT']['providers_dir']
        self.generators_path = self.config['DEFAULT']['generators_dir']

        # Generators pairs each generation module with its config-valid name, for later searching
        self.faker = Faker()
        self.generators = {}

        # Manually reads in all valid .py files in providers/generators, then converts the read directory into
        # an import-valid path in order to manually import them into the generator. Hacky, probably a better
        # way to do it.
        self.load_providers(get_package_from_path(self.providers_path), collect_modules(self.providers_path))
        self.load_generators(get_package_from_path(self.generators_path), collect_modules(self.generators_path))

    def load_providers(self, package, files):
        """
        Imports all providers and adds their functions to faker's library.

        :param package: an import-valid package string
        :param files: the list of files to try to import
        """
        for file in files:
            try:
                module = import_module(package + file[:-3])
                for klass in inspect.getmembers(module, inspect.isclass):
                    if klass[1] is not BaseProvider:
                        self.faker.add_provider(klass[1])
            except ImportError:
                logging.warning("Unable to import provider {}; skipping for now.".format(file[:-3]))

    def load_generators(self, package, files):
        """
        Imports all generators and adds them to the current list of generator modules.

        :param package: an import-valid package string
        :param files: the list of files to try to import
        """
        for file in files:
            try:
                module = import_module(package + file[:-3])
                self.generators[file[:-3]] = module
            except ImportError:
                logging.warning("Unable to import generator {}; skipping for now.".format(file[:-3]))

    def find_template(self, input_file):
        """
        Finds the specified template file in the current template directory.

        :param input_file: the template file to find
        :return: a dictionary containing the contents of the template file
        """
        ext = splitext(input_file)[-1]
        try:
            with open(self.template_path + input_file, 'r') as template:
                if ext == ".json":
                    return json.dump(template)
                elif ext == ".yaml":
                    return yaml.safe_load(template)
                else:
                    logging.error("Unsupported file type {}; skipping this file.".format(ext))
                    return {}
        except yaml.YAMLError:
            logging.error("Unable to load yaml file for section {}.".format(input_file))

    def gen(self, section, template):
        """
        Runs a sample generator for the specified template.

        :param section: the config section of the current template
        :param template: the dict holding the loaded template
        """
        generator = _DEFAULT_GENERATOR
        try:
            generator = self.config[section]['generator']
        except KeyError:
            logging.warning("Unable to find generator listed for dataset {}; using base generator {}.".format(section, _DEFAULT_GENERATOR))

        count = _DEFAULT_COUNT
        try:
            count = int(self.config[section]['count'])
        except KeyError:
            logging.warning(
                "Unable to read count variable listed for dataset {}; using default value of {}.".format(section, _DEFAULT_COUNT))

        func = getattr(self.generators[generator], 'gen')
        return func(self.faker, count, template)

    def gen_all(self):
        """
        Runs a sample generator for each queued template.

        """
        for section in self.config.sections():
            section_config = self.config[section]

            if section_config.getboolean('enabled'):
                template = self.find_template(section_config['input_file'])
                self.write_to_file(section, section_config['output_type'], self.gen(section, template))

    def write_to_file(self, section, output_type, results):
        """
        Writes the generated samples to a file.

        :param section: the config section of the current template
        :param output_type: the output file type
        :param results: the generated samples
        """
        write_location = self.output_path + section

        if output_type == "json":
            with open(write_location + ".json", 'w') as result_file:
                json.dump(results, result_file)
        elif output_type == "yaml":
            with open(write_location + ".yaml", 'w') as result_file:
                yaml.safe_dump(results, result_file)
        elif output_type == "csv":
            pd.DataFrame(results).to_csv(write_location + ".csv", index=False)
        elif output_type == "sample":
            with open(write_location + ".sample", 'w') as result_file:
                json.dump(results, result_file)
        else:
            logging.error("Unrecognized file format {}; defaulting to json for results.".format(output_type))
            with open(write_location + ".json", 'w') as result_file:
                json.dump(results, result_file)

        logging.info("Successfully wrote results to {}!".format(write_location))


if __name__ == "__main__":
    logging.basicConfig(filename="eventgen_sampler.log", level=logging.INFO)

    gs = SampleDataGenerator(*sys.argv[1:])
    gs.gen_all()
