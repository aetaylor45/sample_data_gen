from os import listdir
from os.path import dirname, isfile, splitext

__all__ = []
for module in listdir(dirname(__file__)):
    module_path = splitext(module)
    if module_path[-1] == ".py" and not module.endswith("__init__.py"):
        __all__.append(module_path[-2])
