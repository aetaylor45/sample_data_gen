import logging
from copy import deepcopy


def update_all_items(faker, template):
    for key, value in template.items():
        if isinstance(value, dict):
            update_all_items(faker, value)
        else:
            genned_value = value
            try:
                func = getattr(faker, value[0])
                if len(value) > 1:
                    genned_value = func(*value[1:])
                else:
                    genned_value = func()
                template[key] = genned_value
            except Exception:
                logging.error("Unrecognized function {}".format(value[0]))
    return template


def gen(faker, count, template):
    samples = []
    for x in range(count):
        sample = deepcopy(template)
        samples.append(update_all_items(faker, sample))
    return samples
